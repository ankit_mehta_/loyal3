package FileSystem;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class FileSystem {

	Hashtable<String,Node>path=new Hashtable<String,Node>(); 
	Node root  ;
	Node currentNode ;

	public  FileSystem()
	{

		createTree(root,"/");


	}


	void  createTree(Node root,String rootData) {

		System.out.println("Creating FS");
		root = new Node (rootData,root,new ArrayList<Node>())				;
		currentNode=root;

	}



	void addChildren( Node n,  Node  child)
	{


		n.children.add (child);
	}


	public   class Node  
	{
		public Node(String n,Node p,ArrayList<Node>child)
		{
			name=n;
			parent=p;
			children =child;


		}
		public String name;
		public Node parent;
		public List<Node> children;
		public List<String> files = new ArrayList<String>();


	}





	void mkdir(String cmd)
	{

		ArrayList<Node> folder = new ArrayList<Node>();
		Node n = new Node(cmd,currentNode,folder);

		// folder.add(n);

		addChildren(currentNode,n);




	}

	void cd(String path)
	{

		if(path.equals(".."))
		{
			currentNode=currentNode.parent;
		}
		else
		{
			for(int i=0;i<currentNode.children.size();i++)
			{
				//System.out.println(currentNode.children.get(i).name);
				if(currentNode.children.get(i).name.equals(path))
				{

					currentNode=currentNode.children.get(i);
				}

			}



		}

	}

	void addFiles(Node n , String file)
	{

		n.files.add(file);

	}

	private void touch(String string) 
	{


		addFiles(currentNode, string);


	}

	private void ls()
	{

		System.out.println( );
		for(int i=0;i<currentNode.children.size();i++)
		{
			System.out.println("Dir "+currentNode.children.get(i).name);
		}

		for(int i=0;i<currentNode.files.size();i++)
		{
			System.out.println("File "+currentNode.files.get(i));
		}



	}

	public static void main(String[] args) 
	{
		try {
			FileSystem fs = new FileSystem();
			while (true)
			{


				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				System.out.print("$");

				String input;

				input = br.readLine();


				String[] cmd=input.split(" ") ; 
				if(cmd[0].contains("cd"))
				{
					fs.cd(cmd[1]);

				}

				if(cmd[0].contains("ls"))
				{
					fs.ls( );

				}

				if(cmd[0].contains("touch"))
				{
					fs.touch(cmd[1]);

				}

				if(cmd[0].contains("mkdir"))
				{
					if (cmd.length>1)
						fs.mkdir(cmd[1]);
					else
						System.out.println("Wrong cmd");
				}


			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}



}
